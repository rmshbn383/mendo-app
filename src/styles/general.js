import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  item: {
    paddingHorizontal: 18,
    paddingVertical: 24,
    marginHorizontal: 16,
    borderBottomWidth: 0.5,
    borderBottomColor: "#D9D9D9",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 20,
    color: "#353535",
  },
  sellerService: {
    fontSize: 16,
    color: "#353535",
    opacity: 0.8,
    marginTop: 6,
  },
  openIcon: {
    color: "#353535",
  },
  filter: {
    color: "#353535",
    fontSize: 16,
    flex: 1,
  },
  filterContainer: {
    margin: 12,
    marginBottom: 0,
    paddingLeft: 10,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    height: 50,
    borderRadius: 8,
    borderColor: "#D9D9D9",
    borderWidth: 1,
  },
  loaderContainer: {
    position: "absolute",
    display: "flex",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    backgroundColor: "#4D4D4D80",
    zIndex: 2,
  },
  dialogContainer: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  centerText: { textAlign: "center" },
  fullHeight: {
    height: "100%",
  },
  centerContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    height: "90%",
    alignItems: "center",
  },
  horizontalMargin: {
    marginHorizontal: 10,
  },
  touchSize: {
    height: "100%",
    width: 60,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  listContainer: { flex: 1 }
});

export default styles;
