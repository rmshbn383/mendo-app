/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  FlatList,
} from "react-native";
import Filter from "../components/filter";
import sellerApi from "../api/sellersApi";
import Loader from "../components/loader";
import SellerItem from "../components/sellerItem";
import styles from "../styles/general";


const SellerScreen = ({ navigation }) => {

  const [sellers, setSellers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    sellerApi.getSellers()
      .then(res => {
        setSellers(res.data);
        setLoading(false);
      }).catch(err => {
      setLoading(false);
    });
  }, []);

  const filterSubmit = (value) => {
    setLoading(true);
    sellerApi.getSellers(value)
      .then(res => {
        setSellers(res.data);
        setLoading(false);
      }).catch(err => {
      setLoading(false);
    });
  };

  const renderItem = ({ item }) => (
    <SellerItem title={item.name} service={item.service}
                onPress={() => navigation.navigate("Slots", { sellerId: item._id })} />
  );

  return (
    <SafeAreaView style={styles.listContainer}>
        <Loader loading={loading} />
        <Filter value={""} onSubmit={filterSubmit} />
        <FlatList
          data={sellers}
          renderItem={renderItem}
          keyExtractor={item => item._id}
        />
    </SafeAreaView>
  );
};

export default SellerScreen;
