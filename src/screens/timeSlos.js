/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
} from "react-native";
import slotsApi from "../api/timeSlotsApi";
import Loader from "../components/loader";
import SlotItem from "../components/slotItem";
import styles from "../styles/general";
import Icon from "react-native-vector-icons/FontAwesome";

const TimeSlotsScreen = ({ route }) => {

  const sellerId = route.params.sellerId;

  const [slots, setSlots] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadData = () => {
    slotsApi.getTimeSlots(sellerId)
      .then(res => {
        setSlots(res.data);
        setLoading(false);
      }).catch(err => {
      setLoading(false);
    });
  }

   const bookAppointment = (item) => {
     setLoading(true)
     slotsApi.bookTimeSlot(item._id, item).then(()=>{
       loadData()
     }).catch(()=>{
       setLoading(false)
     })
   };

  useEffect(() => {
    loadData()
  }, []);


  const renderItem = ({ item }) => (
    <SlotItem item={item} bookAppointment={bookAppointment}/>
  );

  return (
    <SafeAreaView>
      <View style={styles.fullHeight}>
        <Loader loading={loading} />
        {
          !loading && slots.length === 0 ?
            <View style={styles.centerContent}>
              <Icon name={"exclamation-circle"} color={"#353535"} size={18} style={styles.horizontalMargin}/>
              <Text style={styles.centerText}>No available appointment!</Text>
            </View>
            :
            <View>
              <FlatList
                data={slots}
                renderItem={renderItem}
                keyExtractor={item => item._id}
              />
            </View>
        }
      </View>
    </SafeAreaView>
  );
};

export default TimeSlotsScreen;
