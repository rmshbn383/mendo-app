/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import type { Node } from "react";
import SellerScreen from "../src/screens/sellers";
import TimeSlotsScreen from "../src/screens/timeSlos";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App: () => Node = () => {

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#284B63',
          },
          headerTitleStyle: {
            alignSelf: 'center',
          },
          headerTintColor: '#D9D9D9',
        }}
      >
        <Stack.Screen
          name="Home"
          component={SellerScreen}
          options={{title: "Mendo"}}
        />
        <Stack.Screen
          name="Slots"
          component={TimeSlotsScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
