import instance from "./instance";

const sellerApi = {
  getSellers: (filter) => instance.get(!!filter ? `sellers?filter=${filter}` : "sellers"),
};

export default sellerApi;
