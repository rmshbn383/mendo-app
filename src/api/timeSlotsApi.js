import instance from "./instance";

const slotsApi = {
  getTimeSlots: (filter) => instance.get(`timeSlots?filter=${filter}`),
  bookTimeSlot: (id, data) => instance.patch(`timeSlots/${id}`, { ...data, isBooked: true }),
};

export default slotsApi;
