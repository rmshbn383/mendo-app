import React, { useState } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import styles from "../styles/general";
import Icon from "react-native-vector-icons/FontAwesome";
import Date from "moment";
import BookingDialog from "./dialog/bookingDialog";
import ResultDialog from "./dialog/resultDialog";

const SlotItem = ({ item, bookAppointment }) => {
  const { day, hour, isBooked } = item;

  const [bookingVisible, setBookingVisible] = useState(false);
  const [resultVisible, setResultVisible] = useState({
    isVisible: false,
    text: "",
  });

  const onPress = () => {
    if (isBooked)
      setResultVisible({ isVisible: true, text: "This appointment is already booked!" });
    else
      setBookingVisible(true);
  };

  return (
    <View style={styles.item}>

      <View>
        <Text style={styles.title}>{Date(day * 1000).format("dddd - DD.MMM.YYYY")}</Text>
        <Text style={styles.sellerService}>{hour}</Text>
      </View>

      <TouchableOpacity onPress={onPress} style={styles.touchSize}>
        <Icon name={"calendar-check-o"} color={isBooked ? "#ba0c2f" : "#77ada9"} size={22} />
      </TouchableOpacity>

      <BookingDialog item={item}
                     visible={bookingVisible}
                     setVisible={setBookingVisible}
                     bookAppointment={bookAppointment}
                     setResultVisible={setResultVisible} />

      <ResultDialog dialogState={resultVisible} setState={setResultVisible} />
    </View>
  );
};

export default SlotItem;
