import React from "react";
import styles from "../../styles/general";
import Dialog from "react-native-dialog";

const ResultDialog = (props) => {

  const { dialogState, setState } = props;

  const handleCancel = () => {
    setState({ isVisible:false, text: "" });
  };

  return (
    <Dialog.Container visible={dialogState.isVisible}>
      <Dialog.Title style={styles.centerText}>{dialogState.text}</Dialog.Title>
      <Dialog.Button label="OK" onPress={handleCancel} />
    </Dialog.Container>
  );
};

export default ResultDialog;
