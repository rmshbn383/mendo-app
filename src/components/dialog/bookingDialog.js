import React from "react";
import styles from "../../styles/general";
import Dialog from "react-native-dialog";
import Date from "moment";

const BookingDialog = (props) => {

  const { item, visible, setVisible, bookAppointment, setResultVisible } = props;
  const formattedDate = Date(item.day * 1000).format("DD.MM.YYYY");

  const handleCancel = () => {
    setVisible(false);
  };

  const handleConfirm = () => {
    bookAppointment(item);
    setVisible(false);
    setResultVisible({ isVisible: true, text: "Appointment has been booked successfully" })
  };

  return (
      <Dialog.Container visible={visible}>
        <Dialog.Title style={styles.centerText}>Book an Appointment</Dialog.Title>
        <Dialog.Description style={styles.centerText}>
          {`Do you want Book this appointment at ${formattedDate} between ${item.hour}?`}
        </Dialog.Description>
        <Dialog.Button label="Cancel" onPress={handleCancel} />
        <Dialog.Button label="Confirm" onPress={handleConfirm} />
      </Dialog.Container>
  );
};

export default BookingDialog;
