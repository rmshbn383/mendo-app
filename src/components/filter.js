import React, { useState } from "react";
import { TextInput, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "../styles/general";

const Filter = (props) => {

  const [value, onChangeValue] = useState(props.value);

  return (
    <View style={styles.filterContainer}>
      <Icon name="search" size={20} color="#D9D9D9" />
      <TextInput
        style={styles.filter}
        onChangeText={onChangeValue}
        value={value}
        placeholder="Seller name or service"
        onSubmitEditing={() => props.onSubmit(value)}
      />
    </View>
  );
};

export default Filter;
