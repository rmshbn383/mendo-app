import React from "react";
import { ActivityIndicator, View } from "react-native";
import styles from "../styles/general";

const Loader = (props) => {
  const { loading } = props;

  return loading ?
    <View style={[styles.loaderContainer]}>
      <ActivityIndicator size="large" color="#3c6e71" />
    </View> : <></>;
};


export default Loader;
