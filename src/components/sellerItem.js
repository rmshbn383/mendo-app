import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import styles from "../styles/general";
import Icon from "react-native-vector-icons/FontAwesome";

const SellerItem = ({ title, service, onPress }) => (
  <TouchableOpacity
    onPress={onPress}
    style={styles.item}>
    <View>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.sellerService}>{service}</Text>
    </View>
    <Icon name={"chevron-right"} style={styles.openIcon} />
  </TouchableOpacity>
);

export default SellerItem;
